# Ukama Devstack Setup

The application will setup and configure all the required software components that are necessary to run and virtualize the Ukama Edge setup


## Configuring and Customizing the setup (Optional)

Set these up to make your setup configurable according to your needs

| Variables                                            | Description                                                                                                                       |
| ---------------------------------------------------- | --------------------------------------------------------------------------------------------------------------------------------- |
| [site_count](./modules/vagrant/variables.tf)         | To Manage number of sites |
| [site_vm_cpu](./modules/vagrant/variables.tf)        | To Manage CPU specs for site |
| [site_vm_memory](./modules/vagrant/variables.tf)     | To Manage Memory for site |
| [nodeA](./modules/libvirt/variables.tf)              | To Manage Name, CPU, Mem for NodeA |
| [nodeT](./modules/libvirt/variables.tf)              | To Manage Name, CPU, Mem for NodeT |
| [nodePS](./modules/libvirt/variables.tf)             | To Manage Name, CPU, Mem for NodePS |



## Technological Stack:
This comprises of the followng main components
   - [Terraform](https://www.terraform.io/)
   - [Vagrant](https://www.vagrantup.com/)
   - [Vagrant Provider](https://github.com/bmatcuk/terraform-provider-vagrant)
   - [Terragrunt](https://github.com/gruntwork-io/terragrunt)
   - [GCP Images](https://cloud.google.com/storage)
   - [Libvirt Provider](https://github.com/dmacvicar/terraform-provider-libvirt)
   - [Virsh](https://libvirt.org/manpages/virsh.html)

## Installation

The installation method is as under:

**Running this Setup**

1. Clone this repository and change directory to `Ukama-devstack`
   
   ```sh 
   git clone https://github.com/Kakapo-Networks/Ukama-devstack.git; cd Ukama-devstack

1. `main.sh` is the main script that will setup all the things for you. If you run `./main.sh` 
    without any flags it will print out `help` or alternatively you can do `./main.sh --help`
    so that you can choose whatever you want to do. 
   
   ```
    ./main.sh
      --Help:     Please specify one of the options to proceed
     OPTIONS     DESCRIPTION
     --setup     Install all pre-requsite tools needed for Ukama setup
     --purge     Purge all pre-requsite tools installed for Ukama setup
     --plan      Show what chagnes or resrouces Terraform will make for Ukama setup  
     --apply     Make those changes physically onn Machine !!
     --destroy   Destroy the resources made by Terraform 
     --download  Download vNodes ISO's from GCP Cloud
     
  **Note: You need to be root to run this script othwerwise the script will display you a message to switch to root user**
 
 1. Run `./main.sh --setup` to install the pre-reqs for the setup to run smoothly.
    This would install the following components, and will make a local file to track
    what has been installed by the script so later when we clean it should remove 
    only what it installed
    ```sh
    - jq
    - git
    - curl
    - wget
    - go
    - virtualbox
    - vagrant
    - virsh
    - terraform
    - terragrunt
    - plugins

 
 1. Now that you have installed the pre-reqs you need to download images of vNodes from GCP.
    This can be done by running `./setup.sh --download`. This will download the following ISO's
    ```
    anode_kernel="https://storage.cloud.google.com/ukama-vnodes-images/aNode/linuxkit-kernel"
    anode_cmdline="https://storage.cloud.google.com/ukama-vnodes-images/aNode/linuxkit-cmdline"
    tnode_kernel=""https://storage.cloud.google.com/ukama-vnodes-images/tNode/linuxkit-kernel""
    tnode_cmdline="https://storage.cloud.google.com/ukama-vnodes-images/tNode/linuxkit-cmdline"
    psnode_kernel=""https://storage.cloud.google.com/ukama-vnodes-images/psNode/linuxkit-kernel""
    psnode_cmdline="https://storage.cloud.google.com/ukama-vnodes-images/psNode/linuxkit-cmdline"
 
 1. Now that you have installed the pre-reqs and downloaded the images you are all set to run `./main.sh plan`.
    This will show you the resrouces that Terraform will make using Vagrant and other providers
 
 1. Once the Terraform Plan is validated, now make those resoruces physically on the mahcine.
    To do this run `./main.sh apply`. This should make all the resources on the machine
 
### Cleanup

This script can clean the installed resoruces by running `./main.sh purge` command.
This would read the locally made file the frist step and see what packages it installed 
and then will remmove all those packages that it had installed



