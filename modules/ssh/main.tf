resource "null_resource" "generate-sshkey" {
    provisioner "local-exec" {
        command = "yes y | ssh-keygen -b 4096 -t rsa -C 'vagrant' -N '' -f ${var.ssh_priv_key} "
    }
}
