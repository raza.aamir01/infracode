
variable "nodeA" {
    type = map
    default = {
        name = "nodeA"
        cpu =  1
        memory = 1024
        kernel = "/opt/vagrant/aNode/linuxkit-kernel"
        initrd = "/opt/vagrant/aNode/linuxkit-initrd.img"
    }
}


variable "nodeT" {
    type = map
    default = {
        name = "nodeT"
        cpu =  1
        memory = 512
        kernel = "/opt/vagrant/tNode/linuxkit-kernel"
        initrd = "/opt/vagrant/tNode/linuxkit-initrd.img"
    }
}

variable "nodePS" {
    type = map
    default = {
        name = "nodePs"
        cpu =  1
        memory = 512
        kernel = "/opt/vagrant/aNode/linuxkit-kernel"
        initrd = "/opt/vagrant/aNode/linuxkit-initrd.img"
    }
}