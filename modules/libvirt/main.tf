
# Create the machine
resource "libvirt_domain" "nodeA" {
    name = var.nodeA["name"]
    memory = var.nodeA["memory"]
    vcpu = var.nodeA["cpu"]
    kernel = var.nodeA["kernel"]
    initrd = var.nodeA["initrd"]

  console {
    type        = "pty"
    target_port = "0"
    target_type = "serial"
  }

  graphics {
    type = "spice"
    listen_type = "address"
    autoport = "true"
  }
}

# Create the machine
resource "libvirt_domain" "nodeT" {
    name = var.nodeT["name"]
    memory = var.nodeT["memory"]
    vcpu = var.nodeT["cpu"]
    kernel = var.nodeT["kernel"]
    initrd = var.nodeT["initrd"]

  console {
    type        = "pty"
    target_port = "0"
    target_type = "serial"
  }

  graphics {
    type = "spice"
    listen_type = "address"
    autoport = "true"
  }
}

# Create the machine
resource "libvirt_domain" "nodePS" {
    name = var.nodePS["name"]
    memory = var.nodePS["memory"]
    vcpu = var.nodePS["cpu"]
    kernel = var.nodePS["kernel"]
    initrd = var.nodePS["initrd"]

  console {
    type        = "pty"
    target_port = "0"
    target_type = "serial"
  }

  graphics {
    type = "spice"
    listen_type = "address"
    autoport = "true"
  }
}



terraform {
  required_version = ">= 0.12"
    }
