output "machine_names" {
    value = "${vagrant_vm.ukama_site_vm.machine_names}"
}

output "ssh_config" {
    value = "${vagrant_vm.ukama_site_vm.ssh_config}"
}