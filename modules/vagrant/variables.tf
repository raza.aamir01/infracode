variable "site_count" {
    default = 1
    description = "Number of sites to provision"
}

variable "site_vm_cpu" {
    default = 4
    description = "Virtual machine cpu requirements"
}

variable "site_vm_memory" {
    default = 4096
    description = "Virtual machine memory requirements in Mbs"
}

variable "ssh_priv_key" {
  type = string
  default = "~/.ssh/id_rsa-terraform-ukama" 
  description = "SSH key path"
}


variable "ssh_pub_key" {
  type = string
  default = "/root/.ssh/id_rsa-terraform-ukama.pub"
  description = "SSH public key path"
}

variable "vagrantSync_Src" {
  type = string
  default = "$HOME/vNodes-images/"
  description = "Host folder to sync "
}

variable "vagrantSync_Dest" {
  type = string
  default = "/opt/vagrant"
  description = "Destination folder to sync "
}

variable "vagrantDir" {
  type = string
  default = "$HOME/vagrant/"
  description = "Directory path where vagrantfile exists"
}