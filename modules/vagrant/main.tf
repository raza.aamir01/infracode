resource "vagrant_vm" "ukama_site_vm" {
  vagrantfile_dir = var.vagrantDir
  env = {
    vm_cpus = var.site_vm_cpu,
    vm_mem = var.site_vm_memory,
    vag_count = var.site_count,
    vag_priv_key = var.ssh_priv_key,
    vag_pub_key = var.ssh_pub_key,
    vag_sync_src = var.vagrantSync_Src,
    vag_sync_dest = var.vagrantSync_Dest,
  }

}



resource "null_resource" "sysprep"{

    count = var.site_count

    provisioner "remote-exec" {

  connection {
    type = vagrant_vm.ukama_site_vm.ssh_config[count.index].type
    user = vagrant_vm.ukama_site_vm.ssh_config[count.index].user
    host = vagrant_vm.ukama_site_vm.ssh_config[count.index].host
    port = vagrant_vm.ukama_site_vm.ssh_config[count.index].port
    private_key = file(var.ssh_priv_key)
  }


    inline = [
      "sudo apt update",
      "sudo apt install lvm2 qemu qemu-kvm libvirt-bin libvirt-dev -y",
      "cat /etc/group | grep libvirt | awk -F':' {'print $1'} | xargs -n1 sudo adduser $USER",
      "sudo adduser $USER kvm",
    ]

  }
}
