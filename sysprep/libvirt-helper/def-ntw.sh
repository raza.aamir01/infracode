#!/bin/bash
set -e

trap 'catch $?' EXIT

catch() {
  local lc="$BASH_COMMAND"
  if [ "$1" != "0" ]; then
    echo "Command $lc exited with exit code error $1"
  fi
}

list=($(sudo ls *.xml))


define () {
    echo "INFO: Defined Network"
    sudo virsh net-define $1

}

start () {
    echo "INFO: Start Network"
    sudo virsh net-start $1

}

auto_start(){
   echo "INFO: Autostart Network"
   sudo  virsh net-autostart $1

}


for f in "${list[@]}";do
    echo $f
    define $f
    ntw=$(echo $f | cut -f 1 -d ".")
    start $ntw
    auto_start $ntw
done
