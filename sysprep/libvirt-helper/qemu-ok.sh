#!/bin/bash

trap 'catch $?' EXIT
catch() {
  local lc="$BASH_COMMAND"
  if [ "$1" != "0" ]; then
    echo "Command $lc exited with Exit Code $1"
  fi
}




kvm_ok () {
  which kvm-ok
  if [ $? -eq 0 ]; then
     echo  "INFO: kvm-ok installed"
  else
     echo "INFO: Installing cpu-checker" 
     sudo apt install cpu-checker -y -q

  fi
}

kvm_ok 


set -e
virt_chk () {
  CPU=$(sudo egrep -c '(vmx|svm)' /proc/cpuinfo)
  KVM=$(sudo kvm-ok | grep not)
  if [ "$CPU" -eq 0 ]
   then
     echo "INFO:Your CPU does not support hardware virtualization";
     exit;
  else [ ! -z "$KVM" ];
      echo "INFO: Your CPU does not support KVM extensions";
     exit;
  fi
}

virt_chk
