#!/bin/bash
set -e

trap 'catch $?' EXIT

catch() {
  local lc="$BASH_COMMAND"
  if [ "$1" != "0" ]; then
    echo "Command $lc exited with exit code error $1"
  fi
}


ntw=$(sudo virsh net-list --all | grep -v "default" | awk '{print $1}' | tail -n +3)
echo -e  "List of networks are \n  $ntw"
arrntw=($ntw)
undefine () {
    echo "INFO: Undefined Network"
    sudo virsh net-undefine $1

}


destroy () {
   echo "INFO: Destroy Network"
   sudo  virsh net-destroy $1

}


for f in "${arrntw[@]}";do
    if [ -z "$f" ]
    then
	 echo "No networks found"
	  exit
    else :	    
    	echo $f
    	undefine $f
    	destroy $f
    fi
done
