#!/usr/bin/python3
import click

"""
This script creates private and public network bridges and makes
certain files on the filesystem in the *.xml format which dynamically
defines the network topology of the setup. This script takes some
arguments if you want to override the default arguments. All the
arguments are required but if they aren't supplied they will use
default values
Example Usage:
-------------
if you want to use default values then
    $ python setup_network.py
    
If you would want to provide custom values
    $ python setup_network.py --sites <number-of-sites> --name <name-for-sites> --public_br <pubic-bridge> --private_br <private-bridge>
For Example:
    $ python setup_network.oy --sites 10 --name kakapo
Note: For now the maximun number for supported sites is 10
"""

HOST_TEMPLATE = """<network>
    {NETWORK_NAME}
    {BRIDGE_NAME}
    {IP_ADDR}
    <dhcp>
    {DHCP}
    </dhcp>
  </ip>
</network>"""

PUBLIC_TEMPLATE = """<network>
  {NETWORK_NAME}
  <forward mode='nat'>
    <nat>
      <port start='1024' end='65535'/>
    </nat>
  </forward>
  {BRIDGE_NAME}
  {IP_ADDR}
    <dhcp>
    {DHCP}
    </dhcp>
  </ip>
</network>"""


def make_network_bridges(name, site_num,
                         bridge, br_name, bridge_type=None):
    """
    :param str name: Name of site
    :param int site_num: Number of sites
    :param str bridge: bridge name
    :param str bridge_type: defaults to None
    :return: network_name, bridge_name, ip_addre, dhcp
    """
    network_name = f"<name>{name}{site_num}{bridge}</name>"

    if bridge_type == "public":
        bridge_name = f"<bridge name='{br_name}{site_num}' stp='on' delay='0'/>"
        ip_addr = f"<ip address='192.168.12{site_num}.1' netmask='255.255.255.0'>"
        dhcp = f"<range start='192.168.12{site_num}.1{site_num}' " \
               f"end='192.168.12{site_num}.254'/>"
    else:
        bridge_name = f"<bridge name='{br_name}{site_num}' stp='on' delay='0'/>"
        ip_addr = f"<ip address='192.168.13{site_num}.1' netmask='255.255.255.0'>"
        dhcp = f"<range start='192.168.13{site_num}.1{site_num}' " \
               f"end='192.168.13{site_num}.254'/>"

    return network_name, bridge_name, ip_addr, dhcp, br_name


def setup_public_bridge(name, site_num, bridge, public_br_name):
    """
    :param str name: Name of the site
    :param int site_num: Site Number
    :param str bridge: The defined public bridge for site
    :param str bridge: The defined public bridge name site
    :return: None
    """

    network_name, bridge_name, ip_addr, dhcp, br_name = make_network_bridges(
        name,
        site_num,
        bridge,
        public_br_name,
        bridge_type="public",
        
    )

    # set name to make xml file
    file_name = name + str(site_num) + bridge

    generate_public_template(
        network_name,
        bridge_name,
        ip_addr,
        dhcp,
        file_name
    )


def setup_private_bridge(name, site_num, bridge, private_br_name):
    """
    :param name:
    :param site_num:
    :param bridge:
    :return: None
    """
    network_name, bridge_name, ip_addr, dhcp, br_name = make_network_bridges(
        name,
        site_num,
        bridge,
        private_br_name
    )

    file_name = name + str(site_num) + bridge

    generate_host_template(
        network_name,
        bridge_name,
        ip_addr,
        dhcp,
        file_name
    )


def generate_public_template(
        network_name,
        bridge_name,
        ip_addr,
        dhcp,
        file_name
):
    """
    :param str network_name: The name of the network
    :param str bridge_name: Name of bridge
    :param str ip_addr: IP address for the bridge
    :param str dhcp: DHCP for the Network
    :param str file_name: File name to generate
    :return: None
    """
    public_virsh = PUBLIC_TEMPLATE.format(
        NETWORK_NAME=network_name,
        BRIDGE_NAME=bridge_name,
        IP_ADDR=ip_addr,
        DHCP=dhcp
    )

    # generate xml file withe supplied parameters
    xmlgen(public_virsh,file_name)


def generate_host_template(network_name,
                           bridge_name,
                           ip_addr,
                           dhcp,
                           file_name):

    """
    :param str network_name: The name of the network
    :param str bridge_name: Name of bridge
    :param str ip_addr: IP address for the bridge
    :param str dhcp: DHCP for the Network
    :param str file_name: File name to generate
    :return: None
    """

    host_virsh = HOST_TEMPLATE.format(
        NETWORK_NAME=network_name,
        BRIDGE_NAME=bridge_name,
        IP_ADDR=ip_addr,
        DHCP=dhcp
    )
    
    # generate xml file withe supplied parameters
    xmlgen(host_virsh, file_name)


def xmlgen(xml, file_name):
    """
    :param str xml: xml template
    :param str file_name: file name to write in
    :return: None
    """
    file_name = file_name + '.xml'
    with open(file_name, "w") as xml_file:
        xml_file.write(xml)


@click.command()
@click.option('--sites', default=1, 
              help='Number of sites to configure')
@click.option('--name', default='site',
              help='Site Name you want to keep e.g Kakapo-CA')
@click.option('--public_br', default='nat',
              help='Public Bridge type for psNode')
@click.option('--private_br', default='private',
              help='Private Bridge type for aNode, tNode')
@click.option('--public_br_name', default='virbr1',
              help='Private Bridge Name for aNode, tNode')
@click.option('--private_br_name', default='virbr2',
              help='Private Bridge Name for aNode, tNode')
def spawn_sites(
        sites, name,
        public_br, private_br,
        public_br_name, private_br_name
):
    """
    :param int sites: No. of sites to spin, defaults to 1
    :param str name:  Name of the site e.g Kakapo-CA, defaults to site
    :param str public_br: Public bridge interface, defaults to nat
    :param str private_br: Private bridge interface, defaults to private
    :param str public_br_name: Public bridge name, defaults to virbr1
    :param str private_br_name: Private bridge name, defaults to virbr2
    :return: None
    """
    for site in range(sites):

        click.echo(f'Site Name: {name}')
        click.echo(f'Site Number: {sites}')
        click.echo(f'Public Bridge for psNode: {public_br}')
        click.echo(f'Private Bridge for aNode,tNode: {private_br}')
        click.echo(f'Public Bridge name : {public_br_name + str(site)}')
        click.echo(f'Private Bridge name: {private_br_name + str(site)}')
        setup_public_bridge(name, site, public_br, public_br_name)
        setup_private_bridge(name, site, private_br, private_br_name)


if __name__ == '__main__':
    spawn_sites()


