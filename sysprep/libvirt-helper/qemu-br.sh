#!/bin/bash
set -e


sudo chmod u+x /usr/lib/qemu/qemu-bridge-helper
sudo mkdir -p /etc/qemu/

echo $1 $2
if [ $# -eq 0 ]
 then
     bridge=$(brctl show | grep -E 'virbr1|virbr2' | awk '{print $1}')
else
     pub=$1
     priv=$2
     bridge=$(brctl show | grep -E 'pub|priv' | awk '{print $1}')
fi

echo $bridge
for br in $bridge
 do 
   echo $br	 
   echo allow $br | sudo  tee -a  /etc/qemu/bridge.conf
   sudo cat /etc/qemu/bridge.conf
 done
