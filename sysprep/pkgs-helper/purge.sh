#!/bin/bash


set -o allexport
[[ -f .env ]] && source .env
set +o allexport

catch() {
  local lc="$BASH_COMMAND"
  if [ "$1" != "0" ]; then
    echo "Command $lc exited with exit code error $1"
  fi
} 

function lvm() {
  echo "INFO: Purging lvm2"
  pkg_name="lvm2"
  apt_purge $pkg_name
  autoremove
}

function virsh() {
  echo "INFO: Purging virsh"
  apt remove --purge qemu qemu-kvm libvirt-bin libvirt-dev virt-manager -y
  autoremove
}



function virtualbox() { 
  echo "INFO: Purging VirtualBox"
  apt_purge $VIRTUALBOX_VER 
  rm_dir $VIRTUALBOX_VMDIR
  rm_dir $VIRTUALBOX_CONFIGDIR
  autoremove
}

function vagrant() {
  echo "INFO: Purging vagrant"
  rm -rf /opt/vagrant
}

function apt_purge(){
  apt remove --purge -yq $@
}



function autoremove() {
  echo "INFO: autoremove"
  apt autoremove -yq
}


function rm_dir() {
  echo "Remove directory"
  rm -rf $@
}


function terraform_plugins() {
  echo "INFO: Purging terraform"
  rm_dir $TERRAFORM_PLUGINS_DIR
}

function uninstall() {
    pkg=$@
    if [ -z $1 ];then
        echo -n "Help: Please specify package name as an argument for purging"
        echo -n "For example: ./purge.sh vagrant virtualbox"
        exit
    fi
    for i in ${pkg[@]}; do 
        cmd=$(which $i)
        if [[ $? -eq 0 ]]; then
            set -e
            trap 'catch $?' EXIT 
            echo "$i installed"
            case "$i" in
                virsh)
                lvm
                $i
                ;;

                terragrunt)
                rm_dir $cmd
                ;;

                virtualbox)
                $i 
                ;;

                terraform) 
                rm_dir $cmd
                terraform_plugins

                ;;

                vagrant) 
                rm_dir $cmd
                apt_purge $i
                ;;

                go) 
                rm_dir $GO_ROOT
                rm_dir $GO_PATH
                ;;

                jq | wget | curl)
                apt_purge $i
                ;;
                
                *)
                echo -n "Unknown Option for action"
                ;;
            esac
            set +e 
        else
            echo "$i not installed"
	          
        fi 
    done
}

uninstall $@



