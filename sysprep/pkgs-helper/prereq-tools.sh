#!/bin/bash


#set -x
#set -o allexport
#[[ -f .env ]] && source .env
#set +o allexport

catch() {
  local lc="$BASH_COMMAND"
  if [ "$1" != "0" ]; then
    echo "Command $lc exited with exit code error $1"
  fi
} 

function download() {
    $i
}

function virtualbox() {
    echo "virtualbox downloading"
    wget -q https://www.virtualbox.org/download/oracle_vbox_2016.asc -O- | sudo apt-key add -
    sudo add-apt-repository "deb [arch=amd64] ${VIRTUALBOX_DL_URL} $(lsb_release -cs) contrib"
    echo "virtualbox installation"
    sudo apt update && sudo apt install virtualbox-6.1 -y
}

function vagrant() {
    echo "vagrant downloading"
    curl -O {$VAGRANT_DL_URL}/{$VAGRANT_VER}/vagrant_{$VAGRANT_VER}_x86_64.deb
    echo "vagrant installation"
    sudo apt install ./*.deb
    rm -rf *.deb
    vag_cmd=$(which vagrant)
    if [ -z $vag_cmd ];then
        echo "Vagrant command not found"
    else   
        $vag_cmd plugin install vagrant-vbguest
    fi
}

function terragrunt() {
    echo "terragrunt downloading"
    wget $TERRAGRUNT_DL_URL/$TERRAGRUNT_VER/terragrunt_linux_amd64
    mv terragrunt_linux_amd64 terragrunt
    chmod +x terragrunt
    cp terragrunt /usr/local/bin
    # Removing locally download terragrunt
    rm -f terragrunt
}


function virsh() {
    echo "Qemu installation"
    sudo apt install lvm2 qemu qemu-kvm libvirt-bin libvirt-dev virt-manager -y
}


function plugins() {
    echo "Checking terraform plugins"
    OS=$(uname -s | tr '[:upper:]' '[:lower:]')
    echo -n "Creating directory at if not exists"
    mkdir -p ${TERRAFORM_PLUGINS_DIR}
    # Terraform vagrant plugin
    vagrant_plugin=${TERRAFORM_PLUGINS_DIR}/terraform-provider-vagrant_v2.0.0
    if [[ ! -f $vagrant_plugin ]]; then
        echo -e "terraform vagrant plugin downloading"
        wget $VAGRANT_PLUGIN_SRC/${VAGRANT_PROV}/terraform-provider-vagrant_${VAGRANT_PROV}_${OS}_amd64.tar.gz
        tar -zxf terraform-provider-vagrant_${VAGRANT_PROV}_${OS}_amd64.tar.gz  --directory ${TERRAFORM_PLUGINS_DIR}/.
        rm -f terraform-provider-vagrant_* 
    else  
        echo -e "terraform vagrant plugin found"     
    fi

    # Terraform libvirt plugin
    libvirt_plugin=${TERRAFORM_PLUGINS_DIR}/terraform-provider-libvirt
    if [[ ! -f  $libvirt_plugin ]]; then
        echo -e "terraform libvirt plugin downloading"
        goexec=$(which go)
        $goexec get $LIBVIRT_PLUGIN_SRC
        $goexec install $LIBVIRT_PLUGIN_SRC
        mv $GOPATH/bin/terraform-provider-libvirt ${TERRAFORM_PLUGINS_DIR}/.

    else  
        echo -e "terraform libvirt plugin found"     
    fi
}

function terraform() {
    echo "terraform installation"
    curl -LO $TERRAFORM_SRC_INSTALLER
    chmod +x terraform-install.sh
    ./terraform-install.sh -i $TERRAFORM_VER
    plugins 
    # Removing locally download terraform script
    rm -f terraform-install.sh
}




function go() {
    PACKAGE_NAME="go$GO_VERSION.linux-amd64.tar.gz"
    TEMP_DIRECTORY=$(mktemp -d)
    if [ "$OS" == "Linux" ] && [ "$ARCH" == "x86_64" ]; then
        echo "Downloading $PACKAGE_NAME ..."
        curl -o "$TEMP_DIRECTORY/go.tar.gz" $GO_DL_URL/$PACKAGE_NAME
        if [ $? -ne 0 ]; then
            echo "Download failed! Exiting."
            exit 1
        fi
        echo "Extracting File..."
        mkdir -p "$GO_ROOT"
        tar -C "$GO_ROOT" --strip-components=1 -xzf "$TEMP_DIRECTORY/go.tar.gz"
        # Remove package from temp directory
        rm -f "$TEMP_DIRECTORY/go.tar.gz"
        # Set GO profile
        if [ -d $GO_PATH ]; then
            mkdir $GO_PATH
        fi
        mkdir -p $GO_PATH/{src,pkg,bin}
        if [ -n "$($SHELL -c 'echo $ZSH_VERSION')" ]; then
            shell_profile="$HOME/.zshrc"
        elif [ -n "$($SHELL -c 'echo $BASH_VERSION')" ]; then
            shell_profile="$HOME/.bashrc"
        fi
        test -e ${shell_profile} | touch ${shell_profile}
        echo "INFO: Set env vairables : $shell_profile"  
        sed -i '/export GOROOT/d' "${shell_profile}"
        sed -i '/$GOROOT\/bin/d' "${shell_profile}"
        sed -i '/export GOPATH/d' "${shell_profile}"
        sed -i '/$GOPATH\/bin/d' "${shell_profile}"                                           
        {
            echo '# Go'
            echo "export GOROOT=${GO_ROOT}"
            echo 'export PATH=$GOROOT/bin:$PATH'
            echo "export GOPATH=$GO_PATH"
            echo 'export PATH=$GOPATH/bin:$PATH'
        } >> "${shell_profile}"
        echo "Go $OS env path setup completed."
        echo -e "\nGo $VERSION was installed into $GOROOT.\nMake sure to relogin into your shell or run:"
        echo -e "\n\tsource $shell_profile\n\nto update your environment variables."
        echo "Tip: Opening a new terminal window usually just works. :)"
    fi
    
}

function install() {
    pkg=$@
    if [ -z $1 ];then
        echo -n "Help: Please specify package name as an argument"
        echo -n "For example: ./prereq-tools.sh vagrant virtualbox"
        exit 
    fi
    for i in ${pkg[@]}; do 
        which $i
        if [[ $? -eq 0 ]]; then 
            echo "PKG $i already installed" 
        else
            set -e
            trap 'catch $?' EXIT
            echo "$i not installed"
	        case "$i" in
                jq | git | wget | curl)
                echo "Use apt for package installing $i"
                apt install $i
                echo "Installed package $i"
                ;;

                vagrant | virtualbox | terraform | terragrunt | go )
                echo "Downloading package $i"
                download $i
                ;;

                virsh)
                echo -n "Qemu installation"
                virsh 
                ;;

                *)
                echo -n "Unknown Option for action"
                ;;
            esac
            set +e
        fi 
    done
}


install $@
