#!/bin/bash
#set -x

set -o allexport
[[ -f .env ]] && source .env 
set +o allexport

catch() {
    local lc="$BASH_COMMAND"
    if [ "$1" != "0" ]; then
        echo "Command $lc exited with exit code error $1"
    fi
} 


function prereq() {
    #Reset state file when tools installed
    test "${bash_trace}" | rm -f "${bash_trace}"
    touch "${bash_trace}"
    
    for i in "${pkgs[@]}"
    do
        echo "${i}"
        bash "${parent_path}"/sysprep/pkgs-helper/prereq-tools.sh "${i}" >> "${bash_trace}" 2>&1 
    done

}

function purge() {
    bash "${parent_path}"/sysprep/pkgs-helper/purge.sh "${@}"
}


function purge_verifier() {
    if [ ! -f "${bash_trace}" ]; then
        echo -e "Note: ** Bash state file at $bash_trace is MANDATORY for --purge operation **"
        exit
    fi
     
    declare -a pkg_ver=()
    for i in "${pkgs[@]}"
    do
        if grep -q "$i not installed" $bash_trace ; then
            echo $installedpkg
            echo -e "$i installed by Ukama setup"
            pkg_ver+=($i)
        else
            echo -e "$i Not installed by Ukama setup" 
            
        fi
    done
    echo $pkg_ver
    if (( ${#pkg_ver[@]} ))
    then
        echo -e "Warning:  These list of packages needs to purge by Ukama setup ${pkg_ver[@]} "
        read -n 1 -s -r -p "Warning: Press any key to CONTINUE CTRL-C to CANCEL"
        purge "${pkg_ver[@]}"
    else
        echo -e "No packages needs to be purged by Ukama setup"
    fi    
}       




function check_root() {
    if [ "$EUID" -ne 0 ];then 
        echo "Please run as root"
        exit
    fi
}


function download() {
    urls=$@
    echo $urls
    for url in ${urls[@]}; do
        case "$url" in
            *aNode* ) 
            echo "Downloading aNode iso at $path/aNode"
            wget -nc $url -P $iso_path/aNode
            ;;

            *tNode* )
            echo "Downloading tNode iso at $path/tNode"
            wget -nc $url -P $iso_path/tNode
            ;;

            *psNode* ) 
            echo "Downloading tNode iso at $path/psNode"
            wget -nc $url -P $iso_path/psNode
            ;;
            esac
    done
     

}


function libvirt() {
    echo "in libvirt function"
    terr_mod_local_path="$parent_path/modules/vagrant"
    terr_mod_libvirt_path="$parent_path/modules/libvirt"
    exe_tool="terragrunt"
    providers_count=$(cd $terr_mod_local_path && $exe_tool output -json machine_names | jq length)
    echo $providers_count
    for i in $(seq $providers_count)
    do
        echo "$i"
        host_vm_name=$(cd "${terr_mod_local_path}" && "${exe_tool}" output -json machine_names | jq -r '.['$i-1']')
        host_vm_port=$(cd "${terr_mod_local_path}" && "${exe_tool}" output -json ssh_config | jq -r '.['$i-1'].port')
        host_vm_ip=$(cd "${terr_mod_local_path}" && "${exe_tool}" output -json ssh_config | jq -r '.['$i-1'].host')
        host_vm_username=$(cd "${terr_mod_local_path}" && "${exe_tool}" output -json ssh_config | jq -r '.[0].user')
        echo "${host_vm_name}" "${host_vm_port}" "${host_vm_ip}" "${host_vm_username}"
        sed  -e "s/host_vm_username/$host_vm_username/g"  -e "s/host_vm_ip/$host_vm_ip/g" -e "s/host_vm_port/$host_vm_port/g" -e "s,keyfile,keyfile=$TF_VAR_ssh_priv_key,g" $parent_path/sysprep/templates/terragrunt-template >  terragrunt.hcl
        terragrunt init
        cd "${terr_mod_libvirt_path}" && terragrunt apply -auto-approve
    done
}


function help_msg() {
    echo -e "--Help:     Please specify one of the options to proceed "
    echo -e "OPTIONS     DESCRIPTION       "
    echo -e "--setup     Install all prerequsite tools needed for ukama setup  "
    echo -e "--purge     Purge all prerequsite tools installed by ukama setup"
    echo -e "--plan      Show what terraform thinks it will do"
    echo -e "--apply     Have terraform do the things. This will cost money."
    echo -e "--destroy   Destroy the things"
    echo -e "--download  Downloading vNodes iso from GCP Cloud "
}


function ter_resource_clean() {
    rm -rf ~/VirtualBox\ VMs/*
    rm -rf "${parent_path}"/modules/ssh/.terr*
    rm -rf "${parent_path}"/modules/vagrant/.terr*
    rm -rf "${parent_path}"/modules/libvirt/.terr*
}

function main() {
    echo "$0"
    pkg="$@"
    if [ -z "$1" ];then
        help_msg
        exit 
    fi
    for i in "${pkg[@]}"; do 
        set -e
        trap 'catch $?' EXIT
	    case "$i" in
            --setup )
            echo -e "Checking Prerqs tools"
            prereq 
            ;;

            --purge )
            echo -e "Checking pre-installed tools setup by Ukama setup and then purge"
            purge_verifier
            ;;

            --download)
            echo -e "Downloading vNodes iso from GCP Cloud"
            declare -a iso_urls=()
            iso_urls+=( $anode_kernel $anode_initrd $tnode_kernel $tnode_initrd $psnode_kernel $psnode_initrd )
            download "${iso_urls[@]}"
            ;;

            --plan)
            echo -e " Show what terraform thinks it will do"
            terragrunt plan  --terragrunt-working-dir modules/ssh 
            terragrunt plan  --terragrunt-working-dir modules/vagrant 
            ;;


            --apply)
            echo -e "Have terraform do the things. This will cost money"
            terragrunt apply -auto-approve --terragrunt-working-dir modules/ssh 
            terragrunt apply -auto-approve --terragrunt-working-dir modules/vagrant
            libvirt  
            ;;

            --destroy)
            echo -e "Destroy the things"
            terragrunt destroy  --terragrunt-working-dir modules/ssh 
            terragrunt destroy  --terragrunt-working-dir modules/vagrant
            ter_resource_clean
            ;;

            --help)
            help_msg
            ;;
            
            *)
            echo -e "Unknown Option for action"
            help_msg
            ;;
            esac
            set +e
     
    done
}

check_root
main "${@}"
